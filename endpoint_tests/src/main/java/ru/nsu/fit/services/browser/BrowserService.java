package ru.nsu.fit.services.browser;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.nsu.fit.services.log.Logger;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class BrowserService {

    static class SpyBrowser extends Browser
    {
        public void jsrenderedwait()
        {
            try
            {
                Thread.sleep(250);
            }
            catch (Exception e)
            {

            }
        }

        @Override
        public Browser click(By element) {
            Logger.info("User is about to click "+element.toString());
            Logger.addScreenshot(this.makeScreenshot());
            Browser t=super.click(element);

            jsrenderedwait();

            Logger.info("User clicked "+element.toString());
            Logger.addScreenshot(this.makeScreenshot());
            return t;
        }

        @Override
        public Browser click(WebElement element) {
            Logger.info("User is about to click "+element.toString());
            Logger.addScreenshot(this.makeScreenshot());
            Browser t=super.click(element);

            jsrenderedwait();

            Logger.info("User clicked "+element.toString());
            Logger.addScreenshot(this.makeScreenshot());
            return t;
        }

        @Override
        public Browser typeText(By element, String text) {
            Logger.info("User is typing "+text+" to "+element.toString());
            return super.typeText(element, text);
        }
    }

    public static Browser openNewBrowser() {
        return new SpyBrowser();
    }
}
