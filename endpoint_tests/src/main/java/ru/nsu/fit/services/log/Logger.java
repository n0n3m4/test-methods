package ru.nsu.fit.services.log;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.PropertyConfigurator;
import org.testng.Reporter;

public class Logger {
    private static final org.apache.log4j.Logger LOGGER;

    static {
        PropertyConfigurator.configure(Logger.class.getClassLoader().getResourceAsStream("log4.properties"));
        LOGGER = org.apache.log4j.Logger.getLogger(Logger.class.getName());
    }

    public static org.apache.log4j.Logger getLogger() {
        return LOGGER;
    }

    public static void addScreenshot(byte[] img)
    {
        Reporter.log("<img src='data:image/png;base64,"+Base64.encodeBase64String(img)+ "'  /><br>");
    }

    public static void reportString(String s)
    {
        Reporter.log(s+"<br>");
    }

    public static void error(String message, Throwable t) {
        reportString(message);
        getLogger().error(message, t);
    }

    public static void debug(String message, Throwable t) {
        reportString(message);
        getLogger().debug(message, t);
    }

    public static void warn(String message, Throwable t) {
        reportString(message);
        getLogger().warn(message, t);
    }

    public static void error(String message) {
        reportString(message);
        getLogger().error(message);
    }

    public static void warn(String message) {
        reportString(message);
        getLogger().warn(message);
    }

    public static void info(String message) {
        reportString(message);
        getLogger().info(message);
    }

    public static void debug(String message) {
        reportString(message);
        getLogger().debug(message);
    }
}
