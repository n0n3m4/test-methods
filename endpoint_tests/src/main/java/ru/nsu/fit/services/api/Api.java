package ru.nsu.fit.services.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.lang3.Validate;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import ru.nsu.fit.services.log.Logger;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class Api {
    String URL;
    String[] auth;
    String lastresponse=null;

    public Api(String URL,String[] auth)
    {
        this.URL=URL;
        this.auth=auth;
    }

    public Response postJson(String method,JsonObject o)
    {
        ClientConfig clientConfig = new ClientConfig();
        if (auth!=null) {
            HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(auth[0], auth[1]);
            clientConfig.register(feature);
        }
        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );
        WebTarget webTarget = client.target(URL).path(method);

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Logger.debug(String.format("Try to make POST at URL %s...",webTarget.getUri()));
        Logger.debug(String.format("With data %s",o.toString()));
        Response response = invocationBuilder.post(Entity.entity(o.toString(),MediaType.APPLICATION_JSON));
        lastresponse=response.readEntity(String.class);
        Logger.info("Response: " + lastresponse);
        Logger.info("Status: " + response.getStatus() + " " + response.getStatusInfo());
        return response;
    }

    public Response getJson(String method)
    {
        ClientConfig clientConfig = new ClientConfig();
        if (auth!=null) {
            HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(auth[0], auth[1]);
            clientConfig.register(feature);
        }
        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );
        WebTarget webTarget = client.target(URL).path(method);

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Logger.debug(String.format("Try to make GET at URL %s...",webTarget.getUri()));
        Response response = invocationBuilder.get();
        lastresponse=response.readEntity(String.class);
        Logger.info("Response: " + lastresponse);
        Logger.info("Status: " + response.getStatus() + " " + response.getStatusInfo());
        return response;
    }

    public JsonObject lastResponseAsJSON()
    {
        return new JsonParser().parse(lastresponse).getAsJsonObject();
    }

    public JsonArray lastResponseAsJSONArray()
    {
        return new JsonParser().parse(lastresponse).getAsJsonArray();
    }
}
