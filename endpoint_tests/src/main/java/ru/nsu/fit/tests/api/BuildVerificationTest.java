package ru.nsu.fit.tests.api;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.lang3.Validate;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.api.Api;
import ru.nsu.fit.services.log.Logger;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class BuildVerificationTest {

    public final int HTTP_OK=200;
    public final String URL="http://localhost:8080/endpoint/rest";

    String username;
    String userid;
    final String password="password123";
    @BeforeClass
    public void initall()
    {
        username=UUID.randomUUID() + "@local.host";
    }

    @Test(description = "Create customer via API.")
    public void createCustomer() {
        JsonObject json = new JsonObject();
        json.addProperty("firstName", "Johnds");
        json.addProperty("lastName", "Weak");
        json.addProperty("login", username);
        json.addProperty("pass", password);
        json.addProperty("balance", "0");

        Response response = new Api(URL,new String[]{"admin","setup"}).postJson("create_customer",json);
        Validate.isTrue(response.getStatus()==HTTP_OK);
    }

    @Test(description = "Try to log into the system.", dependsOnMethods = "createCustomer")
    public void login() {
        Api api=new Api(URL,new String[]{username,password});
        Response response = api.getJson("get_role");
        Validate.isTrue(response.getStatus()==HTTP_OK);
        Validate.isTrue(api.lastResponseAsJSON().get("role").getAsString().equals("USER"));
    }

    @Test(description = "Try to get user id by login.", dependsOnMethods = "createCustomer")
    public void getId()
    {
        Api api=new Api(URL,new String[]{"admin","setup"});
        Response response = api.getJson("get_customer_id/"+username);
        Validate.isTrue(response.getStatus()==HTTP_OK);
        userid=api.lastResponseAsJSON().get("id").getAsString();
    }

    @Test(description = "Try to find created user in the list.", dependsOnMethods = "createCustomer")
    public void getListAndFind()
    {
        Api api=new Api(URL,new String[]{"admin","setup"});
        Response response = api.getJson("get_customers");
        Validate.isTrue(response.getStatus()==HTTP_OK);
        boolean found=false;
        for (JsonElement j:api.lastResponseAsJSONArray())
        {
            if (j.getAsJsonObject().get("login").getAsString().equals(username))
            {
                found=true;
            }
        }
        Validate.isTrue(found);
    }

    @Test(description = "Increase balance of a user.", dependsOnMethods = {"getListAndFind","login","getId"})
    public void incBalance()
    {
        JsonObject json = new JsonObject();
        json.addProperty("id", userid);
        json.addProperty("amount", 1337);

        Response response = new Api(URL,new String[]{"admin","setup"}).postJson("balance_up_customer",json);
        Validate.isTrue(response.getStatus()==HTTP_OK);
    }

    @Test(description = "Test that the balance of a user actually changed.", dependsOnMethods = {"incBalance"})
    public void checkBalance()
    {
        Api api=new Api(URL,new String[]{"admin","setup"});
        Response response = api.getJson("get_customers");
        Validate.isTrue(response.getStatus()==HTTP_OK);
        JsonObject found=null;
        for (JsonElement j:api.lastResponseAsJSONArray())
        {
            if (j.getAsJsonObject().get("login").getAsString().equals(username))
            {
                found=j.getAsJsonObject();
            }
        }
        Validate.isTrue(found!=null);
        Validate.isTrue(found.get("balance").getAsInt()==1337);
    }

    @Test(description = "Delete the user.", dependsOnMethods = {"checkBalance"})
    public void delUser()
    {
        Api api=new Api(URL,new String[]{"admin","setup"});
        JsonObject json = new JsonObject();
        json.addProperty("id", userid);
        Response response = api.postJson("remove_customer",json);
        Validate.isTrue(response.getStatus()==HTTP_OK);
    }

    @Test(description = "Check that user is no longer here.", dependsOnMethods = "delUser")
    public void checkUserNotHere()
    {
        Api api=new Api(URL,new String[]{"admin","setup"});
        Response response = api.getJson("get_customers");
        Validate.isTrue(response.getStatus()==HTTP_OK);
        boolean found=false;
        for (JsonElement j:api.lastResponseAsJSONArray())
        {
            if (j.getAsJsonObject().get("login").getAsString().equals(username))
            {
                found=true;
            }
        }
        Validate.isTrue(!found);
    }
}
