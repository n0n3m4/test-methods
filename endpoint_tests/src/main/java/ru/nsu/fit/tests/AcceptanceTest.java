package ru.nsu.fit.tests;

import org.apache.commons.lang3.Validate;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class AcceptanceTest {
    private Browser browser = null;
    private String fname;
    private String lname;
    private String planame;

    public void jsrenderedwait()
    {
        try
        {
            Thread.sleep(250);
        }
        catch (Exception e)
        {

        }
    }

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
        Random r=new Random();
        fname = "F";
        lname = "L";
        planame = "P";
        for (int i=0;i<10;i++)
            fname+=(char)('a'+r.nextInt(26));
        for (int i=0;i<10;i++)
            lname+=(char)('a'+r.nextInt(26));
        for (int i=0;i<20;i++)
            planame+=(char)('a'+r.nextInt(26));


    }

    @Test
    @Title("Create customer")
    @Description("Create customer via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer feature")
    public void createCustomer() {
        //Browser browser = BrowserService.openNewBrowser();

        // login to admin cp
        browser.openPage("http://localhost:8080/endpoint");
        browser.waitForElement(By.id("email"));

        browser.typeText(By.id("email"),"admin");
        browser.typeText(By.id("password"),"setup");

        browser.click(By.id("login"));

        jsrenderedwait();

        // create customer
        browser.click(By.id("add_new_customer"));

        browser.typeText(By.id("first_name_id"),fname);
        browser.typeText(By.id("last_name_id"),lname);
        browser.typeText(By.id("email_id"),"email@example.com");
        browser.typeText(By.id("password_id"),"strongpass");

        browser.click(By.id("create_customer_id"));
        jsrenderedwait();
    }

    @Test(dependsOnMethods = "createCustomer")
    @Title("Check login")
    @Description("Check that customer is actually listed")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void checkCustomer() {
        List<WebElement> lst=browser.getElement(By.id("customer_list_id")).findElement(By.tagName("tbody")).findElement(By.tagName("tr")).findElements(By.tagName("td"));
        Validate.isTrue(lst.get(1).getText().equals(fname));
        Validate.isTrue(lst.get(2).getText().equals(lname));
    }

    @Test(dependsOnMethods = "checkCustomer")
    @Title("Check balance")
    @Description("Check that we can add balance")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void checkAddBalance() {
        WebElement tr=browser.getElement(By.id("customer_list_id")).findElement(By.tagName("tbody")).findElement(By.tagName("tr"));
        browser.click(tr.findElement(By.tagName("button")));

        jsrenderedwait();

        List<WebElement> lst=browser.getElement(By.id("customer_list_id")).findElement(By.tagName("tbody")).findElement(By.tagName("tr")).findElements(By.tagName("td"));
        Validate.isTrue(lst.get(5).getText().equals("1337"));
    }

    @Test(dependsOnMethods = "checkAddBalance")
    @Title("Check delete")
    @Description("Check that we can delete user")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void checkDelete() {
        WebElement tr=browser.getElement(By.id("customer_list_id")).findElement(By.tagName("tbody")).findElement(By.tagName("tr"));
        browser.click(tr.findElements(By.tagName("button")).get(1));

        jsrenderedwait();

        String t=browser.getElement(By.id("customer_list_id")).findElement(By.tagName("tbody")).getText();
        Validate.isTrue(!t.contains(fname));
    }

    //Everything is linear for convenience
    @Test(dependsOnMethods = "checkDelete")
    @Title("Check create")
    @Description("Check that we can create plan")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Plan feature")
    public void checkCreatePlan() {
        browser.click(By.tagName("a"));
        browser.waitForElement(By.id("add_new_plan"));

        browser.click(By.id("add_new_plan"));
        browser.waitForElement(By.id("create_plan_id"));

        browser.typeText(By.id("name_id"),planame);
        browser.typeText(By.id("details_id"),"Just a test plan");
        browser.typeText(By.id("fee_id"),"123");

        browser.click(By.id("create_plan_id"));
        jsrenderedwait();
    }

    @Test(dependsOnMethods = "checkCreatePlan")
    @Title("Check list")
    @Description("Check that we can see the created plan")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Plan feature")
    public void checkListPlan() {
        List<WebElement> lst=browser.getElement(By.id("plan_list_id")).findElement(By.tagName("tbody")).findElement(By.tagName("tr")).findElements(By.tagName("td"));
        Validate.isTrue(lst.get(1).getText().equals(planame));
    }


    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
}
