package ru.nsu.fit.endpoint.service.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;

import java.util.UUID;

public class CustomerManagerTest {
    private DBService dbService;
    private Logger logger;
    private CustomerManager customerManager;

    private Customer customerBeforeCreateMethod;
    private Customer customerAfterCreateMethod;

    @Before
    public void before() {
        // create stubs for the test's class
        dbService = Mockito.mock(DBService.class);
        logger = Mockito.mock(Logger.class);

        customerBeforeCreateMethod = new Customer()
                .setId(null)
                .setFirstName("John")
                .setLastName("Wick")
                .setLogin("john_wick@gmail.com")
                .setPass("Baba_Jaga")
                .setBalance(0);
        customerAfterCreateMethod = customerBeforeCreateMethod.clone();
        customerAfterCreateMethod.setId(UUID.randomUUID());

        Mockito.when(dbService.createCustomer(customerBeforeCreateMethod)).thenReturn(customerAfterCreateMethod);

        // create the test's class
        customerManager = new CustomerManager(dbService, logger);
    }

    @Test
    public void testCreateNewCustomer() {
        // Вызываем метод, который хотим протестировать
        Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);

        // Проверяем результат выполенния метода
        Assert.assertEquals(customer.getId(), customerAfterCreateMethod.getId());

        // Проверяем, что метод мока базы данных был вызван 1 раз
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().size());
    }

    @Test
    public void testCreateCustomerWithNullArgument() {
        try {
            customerManager.createCustomer(null);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Argument 'customerData' is null.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithEasyPassword()
    {
        for (String s:new String[]{"123qwe","1q2w3e"})
            try {
                customerBeforeCreateMethod.setPass(s);
                customerManager.createCustomer(customerBeforeCreateMethod);
                Assert.fail();
            } catch (IllegalArgumentException ex) {
                Assert.assertEquals("Password is easy.", ex.getMessage());
            }
    }

    @Test
    public void testCreateCustomerWithEmailPassword()
    {
        try {
            customerBeforeCreateMethod.setLogin("t@t.com").setPass("at@t.coma");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Password contains other field.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithLongPassword()
    {
        try {
            customerBeforeCreateMethod.setPass("4ccca52e85faa");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Password's length should be more or equal 6 symbols and less or equal 12 symbols.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithShortPassword()
    {
        try {
            customerBeforeCreateMethod.setPass("4ccca");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Password's length should be more or equal 6 symbols and less or equal 12 symbols.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithZeroMoney()
    {
        try {
            customerBeforeCreateMethod.setBalance(1);
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Wrong balance.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithLoginNotEmail()
    {
        try {
            customerBeforeCreateMethod.setLogin("definitelynotaemail");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Login is not email.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithWrongFirstName()
    {
        for (String s:new String[]{"IVan","ivan","Iv4n","I van"})
        try {
            customerBeforeCreateMethod.setFirstName(s);
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("FirstName is incorrect.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithTooLongFirstName()
    {
        try {
            customerBeforeCreateMethod.setFirstName("Aaaaaaaaaaaaaa");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("FirstName length should be in [2,12] range.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithTooShortFirstName()
    {
        try {
            customerBeforeCreateMethod.setFirstName("A");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("FirstName length should be in [2,12] range.", ex.getMessage());
        }
    }


}
