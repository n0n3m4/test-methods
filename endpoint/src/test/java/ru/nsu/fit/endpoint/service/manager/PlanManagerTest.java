package ru.nsu.fit.endpoint.service.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;

import java.util.UUID;

public class PlanManagerTest {
    private DBService dbService;
    private Logger logger;
    private PlanManager planManager;

    private Plan planBeforeCreateMethod;
    private Plan planAfterCreateMethod;

    @Before
    public void before() {
        // create stubs for the test's class
        dbService = Mockito.mock(DBService.class);
        logger = Mockito.mock(Logger.class);

        planBeforeCreateMethod = new Plan();
        planBeforeCreateMethod.setId(null);
        planBeforeCreateMethod.setName("A simple plan");
        planBeforeCreateMethod.setFee(1337);
        planBeforeCreateMethod.setDetails("This plan is about just taking money and giving nothing in return");
        planAfterCreateMethod = planBeforeCreateMethod.clone();
        planAfterCreateMethod.setId(UUID.randomUUID());

        Mockito.when(dbService.createPlan(planBeforeCreateMethod)).thenReturn(planAfterCreateMethod);

        // create the test's class
        planManager = new PlanManager(dbService, logger);
    }

    @Test
    public void testCreateNewCustomer() {
        // Вызываем метод, который хотим протестировать
        Plan plan = planManager.createPlan(planBeforeCreateMethod);

        // Проверяем результат выполенния метода
        Assert.assertEquals(plan.getId(), planAfterCreateMethod.getId());

        // Проверяем, что метод мока базы данных был вызван 1 раз
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().size());
    }

    @Test
    public void testCreatePlanWithNameWrongSize()
    {
        for (String name:new String[]{"A","AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"})
            try {
                planBeforeCreateMethod.setName(name);
                planManager.createPlan(planBeforeCreateMethod);
                Assert.fail();
            } catch (IllegalArgumentException ex) {
                Assert.assertEquals("Plan name is not in [2,128] range.", ex.getMessage());
            }
    }

    @Test
    public void testCreatePlanWithNameSpecialSymbols()
    {
        for (String name:new String[]{"A<script","'or'1'='1"})
            try {
                planBeforeCreateMethod.setName(name);
                planManager.createPlan(planBeforeCreateMethod);
                Assert.fail();
            } catch (IllegalArgumentException ex) {
                Assert.assertEquals("Plan name contains special characters.", ex.getMessage());
            }
    }

    @Test
    public void testCreatePlanWithDetailsWrongSize()
    {
        for (String details:new String[]{"",new String(new char[1025]).replace("\0", "A")})
            try {
                planBeforeCreateMethod.setDetails(details);
                planManager.createPlan(planBeforeCreateMethod);
                Assert.fail();
            } catch (IllegalArgumentException ex) {
                Assert.assertEquals("Plan details is not in [1,1024] range.", ex.getMessage());
            }
    }

    @Test
    public void testCreatePlanWithWrongFee()
    {
        for (Integer fee:new Integer[]{-1,1000000})
            try {
                planBeforeCreateMethod.setFee(fee);
                planManager.createPlan(planBeforeCreateMethod);
                Assert.fail();
            } catch (IllegalArgumentException ex) {
                Assert.assertEquals("Plan fee is not in [0,999999] range.", ex.getMessage());
            }
    }

}
