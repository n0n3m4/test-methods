$(document).ready(function(){
    $("#add_new_customer").click(function() {
        $.redirect('/endpoint/add_customer.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });

    $.get({
        url: 'rest/get_customers',
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
        }
    }).done(function(data) {
        var json = $.parseJSON(data);

        var dataSet = []
        for(var i = 0; i < json.length; i++) {
            var obj = json[i];
            dataSet.push([i,obj.firstName, obj.lastName, obj.login, obj.pass, obj.balance, obj.id, obj.id])
        }

        //$("#customer_list_id").html(data);
        var table = $('#customer_list_id')
            .DataTable({
                data: dataSet,
                columns: [
					{ title: "#" },
                    { title: "First Name" },
                    { title: "Last Name" },
                    { title: "Email" },
                    { title: "Pass" },
                    { title: "Balance" },
					{ title: "" },
					{ title: "" }
                ],
				columnDefs: [ {
					"targets": [ -2 ],
					"data": null,
					"defaultContent": "<button class='addbtn'>Donate $1337</button>"
				},
				{
					"targets": [ -1 ],
					"data": null,
					"defaultContent": "<button class='delbtn'>Delete user</button>"
				}],
				order: [[0,'desc']]
            });
			
		$('#customer_list_id tbody').on( 'click', '.addbtn', function () {
				var data = table.row( $(this).parents('tr') ).data();				
				$.post({
					url: 'rest/balance_up_customer',
					headers: {
						'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
					},
					contentType : 'application/json',
					data: JSON.stringify({"amount":1337,"id":data[6]})
				});
				location.reload();
		} );
		
		$('#customer_list_id tbody').on( 'click', '.delbtn', function () {
				var data = table.row( $(this).parents('tr') ).data();
				$.post({
					url: 'rest/remove_customer',
					headers: {
						'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
					},
					contentType : 'application/json',
					data: JSON.stringify({"id":data[7]})
				});
				location.reload();
		} );
    });
});