$(document).ready(function(){
    $("#add_new_plan").click(function() {
        $.redirect('/endpoint/add_plan.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });

    $.get({
        url: 'rest/get_plans',
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
        }
    }).done(function(json) {       

        var dataSet = []
        for(var i = 0; i < json.length; i++) {
            var obj = json[i];
            dataSet.push([i,obj.name, obj.details, obj.fee])
        }

        //$("#plan_list_id").html(data);
        var table = $('#plan_list_id')
            .DataTable({
                data: dataSet,
                columns: [
					{ title: "#" },
                    { title: "Name" },
                    { title: "Details" },
                    { title: "Fee" },                    
                ],
				order: [[0,'desc']]
            });			
    });
});