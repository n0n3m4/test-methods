package ru.nsu.fit.endpoint.service.manager;

import com.mysql.cj.api.io.ValueDecoder;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;

import java.util.List;
import java.util.UUID;

public class CustomerManager extends ParentManager {
    public CustomerManager(DBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Customer. Ограничения:
     * Аргумент 'customerData' - не null;
     * firstName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * lastName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * login - указывается в виде email, проверить email на корректность, проверить что нет customer с таким же email;
     * pass - длина от 6 до 12 символов включительно, не должен быть простым (123qwe или 1q2w3e), не должен содержать части login, firstName, lastName
     * money - должно быть равно 0.
     */

    public Customer createCustomer(Customer customer) {
        Validate.notNull(customer, "Argument 'customerData' is null.");

        Validate.notNull(customer.getPass());
        Validate.isTrue(customer.getPass().length() >= 6 && customer.getPass().length() < 13, "Password's length should be more or equal 6 symbols and less or equal 12 symbols.");
        Validate.isTrue(!customer.getPass().equalsIgnoreCase("123qwe") && !customer.getPass().equalsIgnoreCase("1q2w3e"), "Password is easy.");
        Validate.isTrue(!(customer.getPass().contains(customer.getLogin())||customer.getPass().contains(customer.getLastName())||customer.getPass().contains(customer.getFirstName())),
                "Password contains other field.");
        Validate.isTrue(customer.getFirstName().length()>=2 && customer.getFirstName().length()<=12,"FirstName length should be in [2,12] range.");
        Validate.isTrue(customer.getLastName().length()>=2 && customer.getLastName().length()<=12,"LastName length should be in [2,12] range.");

        Validate.isTrue(customer.getLogin().matches("^.+@.+(\\.[^\\.]+)+$"),"Login is not email.");
        Validate.isTrue(customer.getBalance()==0,"Wrong balance.");

        Validate.isTrue(customer.getFirstName().matches("^[A-Z]{1}[a-z]*$"),"FirstName is incorrect.");

        return dbService.createCustomer(customer);
    }

    /**
     * Метод возвращает список объектов типа customer.
     */
    public List<Customer> getCustomers() {
        return dbService.getCustomers();
    }


    /**
     * Метод обновляет объект типа Customer.
     * Можно обновить только firstName и lastName.
     */
    public Customer updateCustomer(Customer customer) {
        UUID uuid=dbService.getCustomerIdByLogin(customer.getLogin());
        Customer origCustomer=dbService.getCustomerByUUID(uuid);
        Validate.isTrue(origCustomer.getPass().equals(customer.getPass()));
        Validate.isTrue(origCustomer.getBalance()==customer.getBalance());
        dbService.updateCustomerByUUID(uuid,customer);
        return dbService.getCustomerByUUID(uuid);
    }

    public void removeCustomer(UUID id) {
        dbService.removeCustomerByUUID(id);
    }

    /**
     * Метод добавляет к текущему баласу amount.
     * amount - должен быть строго больше нуля.
     */
    public Customer topUpBalance(UUID customerId, int amount) {
        Validate.isTrue(amount>0);

        Customer customer = dbService.getCustomerByUUID(customerId);
        customer.setBalance(customer.getBalance()+amount);
        dbService.updateCustomerByUUID(customerId,customer);

        return dbService.getCustomerByUUID(customerId);
    }
}
